package hello.itemservice.domain.item.groupsPractice;

import hello.itemservice.domain.item.ItemType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

//groups 를 사용할때만 적용해봤던것임
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class ValidatedItem {

    //수정 요구사항 추가 -> http 악의적 변경요청때문에 서버에서 id값 최종검증
    @NotNull(groups = UpdateCheck.class)
    private Long id;

    @NotBlank(groups = {SaveCheck.class,UpdateCheck.class})
    private String itemName;

    @NotNull(groups = {SaveCheck.class,UpdateCheck.class})
    @Range(min=1000,max=1000000,groups = {SaveCheck.class,UpdateCheck.class})
    private Integer price;

    @NotNull(groups = {SaveCheck.class,UpdateCheck.class})
    //수정요구사항 추가 -> 수정시에는 수량이 무제한으로
    @Max(value = 9999, groups = SaveCheck.class) //등록시에만 9999 적용
    private Integer quantity;

    private Boolean open; //판매 여부
    private List<String> regions; //등록 지역
    private ItemType itemType; //상품 종류
    private String deliveryCode; //배송 방식


    public ValidatedItem(String itemName, Integer price, Integer quantity) {
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }
}
