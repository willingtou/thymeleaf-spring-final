package hello.itemservice.domain.item;

import hello.itemservice.domain.item.itemDto.ItemSaveForm;
import hello.itemservice.domain.item.itemDto.ItemUpdateForm;
import lombok.Data;

import java.util.List;

@Data
public class Item {

    private Long id;
    private String itemName;
    private Integer price;
    private Integer quantity;

    private Boolean open; //판매 여부
    private List<String> regions; //등록 지역
    private ItemType itemType; //상품 종류
    private String deliveryCode; //배송 방식

    public Item() {
    }

    public Item(String itemName, Integer price, Integer quantity) {
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }
    public Item(ItemSaveForm form){
        this.itemName = form.getItemName();
        this.price = form.getPrice();
        this.quantity = form.getQuantity();
    }
    public Item(ItemUpdateForm form){
        this.itemName = form.getItemName();
        this.price = form.getPrice();
        this.quantity = form.getQuantity();
    }
    public void setEtc(ItemSaveForm form){
        this.open=form.getOpen();
        this.regions=form.getRegions();
        this.itemType=form.getItemType();
        this.deliveryCode=form.getDeliveryCode();
    }
    public void setEtc(ItemUpdateForm form){
        this.open=form.getOpen();
        this.regions=form.getRegions();
        this.itemType=form.getItemType();
        this.deliveryCode=form.getDeliveryCode();
    }
}
