package hello.itemservice.web.item.controller;

import hello.itemservice.domain.item.*;
import hello.itemservice.domain.item.itemDto.ItemSaveForm;
import hello.itemservice.domain.item.itemDto.ItemUpdateForm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/form/items")
@RequiredArgsConstructor
public class ItemController {

    private final ItemRepository itemRepository;
    //검증기 중복 적용을 피하기위한 주석
//    private final ItemValidator itemValidator;
    private static Map<String,String>  regions = new LinkedHashMap<>();
    private static List<DeliveryCode> deliveryCodes = new ArrayList<>();

    @PostConstruct
    public void initModels() {

        //@ModelAttribute를 호출할때마다 새로 만들어서 넣기때문에 처음부터 만들어논 것들 활용함.
        regions.put("SEOUL", "서울");
        regions.put("BUSAN", "부산");
        regions.put("JEJU", "제주");

        deliveryCodes.add(new DeliveryCode("FAST", "빠른 배송"));
        deliveryCodes.add(new DeliveryCode("NORMAL", "일반 배송"));
        deliveryCodes.add(new DeliveryCode("SLOW", "느린 배송"));
    }

    @GetMapping
    public String items(Model model) {
        List<Item> items = itemRepository.findAll();
        model.addAttribute("items", items);
        return "form/items";
    }

    @GetMapping("/{itemId}")
    public String item(@PathVariable long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "form/item";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("item",new Item());
        return "form/addForm";
    }

    /**
     * 검증부분을 WebDataBinder를 이용해서
     * 해당 컨트롤러에만 검증기를 자동 적용할 수 있게 함 (@Validated를 사용가능)
     * beanValidation을 이용 -> 검증기 중복을 피하기위해 주석처리
     */
    /*
    @InitBinder
    public void init(WebDataBinder dataBinder) {
        log.info("init binder {}", dataBinder);
        dataBinder.addValidators(itemValidator);
    }
     */
    @PostMapping("/add")
    public String addItem(@Validated @ModelAttribute("item") ItemSaveForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes) {


        log.info("item.open={}",form.getOpen());
        log.info("item.regions={}",form.getRegions());
        log.info("item.itemType={}", form.getItemType());


        /**
         * //분리한 검증기에서 검증로직 실행 -> 검증관련부분이 복잡해지면 분리해야함
         * itemValidator.validate(item,bindingResult);
         */

        //특정 필드 예외가 아닌 전체 예외
        //글로벌오류는 따로 자바코드로 가져오는것이 좋음.(bean validation을 쓸시)
        if (form.getPrice() != null && form.getQuantity() != null) {
            int resultPrice = form.getPrice() * form.getQuantity();
            if (resultPrice < 10000) {
                bindingResult.reject("totalPriceMin", new Object[]{10000, resultPrice}, null);
            }
        }

        //에러가 있을경우
        if (bindingResult.hasErrors()) {
            log.info("errors={}", bindingResult);
            return "form/addForm";
        }
        //성공 로직
        Item item = new Item(form);
        item.setEtc(form);
        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/form/items/{itemId}";
    }

    @GetMapping("/{itemId}/edit")
    public String editForm(@PathVariable Long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "form/editForm";
    }

    //수정에도 검증기능 추가
    @PostMapping("/{itemId}/edit")
    public String edit(@PathVariable Long itemId, @Validated @ModelAttribute("item") ItemUpdateForm form, BindingResult bindingResult) {

        //특정 필드 예외가 아닌 전체 예외
        //글로벌오류는 따로 자바코드로 가져오는것이 좋음.(bean validation을 쓸시)
        if (form.getPrice() != null && form.getQuantity() != null) {
            int resultPrice = form.getPrice() * form.getQuantity();
            if (resultPrice < 10000) {
                bindingResult.reject("totalPriceMin", new Object[]{10000, resultPrice}, null);
            }
        }
        //에러가 있을경우
        if (bindingResult.hasErrors()) {
            log.info("errors={}", bindingResult);
            //검증오류가 발생하면 editForm으로 이동동
           return "form/editForm";
        }
        Item itemParam = new Item(form);
        itemParam.setEtc(form);
        itemRepository.update(itemId, itemParam);
        return "redirect:/form/items/{itemId}";
    }

    //체크박스 멀티 예시 .. @ModelAttribute의 특별한 사용법
    @ModelAttribute("regions")
    public Map<String, String> regions() {
        return regions;
    }

    //라디오 박스 예시
    @ModelAttribute("itemTypes")
    public ItemType[] itemTypes() {
        return ItemType.values();
    }

    //셀렉트 박스 예시
    @ModelAttribute("deliveryCodes")
    public List<DeliveryCode> deliveryCodes() {
        return deliveryCodes;
    }
}

