package hello.itemservice.web;

import hello.itemservice.domain.member.Member;
import hello.itemservice.domain.member.MemberRepository;
import hello.itemservice.web.argumentresolver.Login;
import hello.itemservice.web.session.SessionConst;
import hello.itemservice.web.session.SessionManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
public class HomeController {

//    private final SessionManager sessionManager;
//    @GetMapping("/")
    public String homeLogin(HttpServletRequest request, Model model) {

        /**
         * 세션매니저를 통해 하던방식
         *         Member member = (Member)sessionManager.getSession(request);
         *
         *         if (member == null) {
         *             return "home";
         *         }
         *         //로그인
         *         model.addAttribute("member", member);
         *         return "loginHome";
         */

        //세션이 없으면 만들지 않고 home 으로 이동
        HttpSession session = request.getSession(false);
        if (session == null) {
            return "home";
        }

        //로그인 시점에 세션에 보관한 회원 객체를 찾는다.
        Member loginMember = (Member) session.getAttribute(SessionConst.LOGIN_MEMBER);
        //세션에 회원 데이터가 없으면 home
        if (loginMember == null) {
            return "home";
        }
        //세션이 유지되면 로그인으로 이동
        model.addAttribute("member", loginMember);
        return "loginHome";
    }

    /**
     @SessionAttribute 라는 스프링이 제공해주는 세션을 편리하게 이용
     세션을 찾고 세션에 들어있는 데이터를 찾는 번거로운 과정을 스프링이 한방에 해결
     */
//    @GetMapping("/")
    public String homeLoginBySpring(@SessionAttribute(name = SessionConst.LOGIN_MEMBER, required = false) Member loginMember, Model model) {

        if (loginMember == null) {
            return "home";
        }
        //세션이 유지되면 로그인으로 이동
        model.addAttribute("member", loginMember);
        return "loginHome";
    }

    /**
     * 직접만든 @Login 애노테이션과
     * ArgumentResolver를 활용한 로그인방식
     */
    @GetMapping("/")
    public String homeLoginV3ArgumentResolver(@Login Member loginMember, Model model) {

        //세션에 회원 데이터가 없으면 home
        if (loginMember == null) {
            return "home";
        }

        //세션이 유지되면 로그인으로 이동
        model.addAttribute("member", loginMember);
        return "loginHome";
    }

}
